import 'package:flutter/material.dart';

class Setting extends StatefulWidget {
  const Setting({Key? key}) : super(key: key);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple[300],
        title: Text("Settings"),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(20),
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.check_circle_outline_rounded),
            onPressed: () {},
          )
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.deepPurple[300],
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.home_rounded,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/beranda');
              },
            ),
            IconButton(
              icon: Icon(Icons.add_shopping_cart_rounded, color: Colors.white),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.shop_2, color: Colors.white),
              onPressed: () {
                Navigator.pushNamed(context, '/tokosaya');
              },
            ),
            IconButton(
              icon: Icon(Icons.person_outlined, color: Colors.white),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
