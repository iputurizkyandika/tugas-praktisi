import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TokoSaya extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple[300],
          title: Text(
            "Toko Saya",
            textAlign: TextAlign.center,
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                Navigator.pushNamed(context, '/setting');
              },
            )
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          color: Colors.deepPurple[300],
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.home_rounded,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/beranda');
                },
              ),
              IconButton(
                icon:
                    Icon(Icons.add_shopping_cart_rounded, color: Colors.white),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.shop_2, color: Colors.white),
                onPressed: () {
                  Navigator.pushNamed(context, '/tokosaya');
                },
              ),
              IconButton(
                icon: Icon(Icons.person_outlined, color: Colors.white),
                onPressed: () {},
              ),
            ],
          ),
        ),
        body: SafeArea(
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.deepPurple[300],
                    image: DecorationImage(
                        image: AssetImage(""), fit: BoxFit.cover)),
                child: Container(
                  width: double.infinity,
                  height: 140,
                  child: Container(
                    alignment: Alignment(0.0, 2.5),
                    child: CircleAvatar(
                      backgroundImage: AssetImage("asset/profile1.png"),
                      radius: 60.0,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Text(
                "Bahagia Store",
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.blueGrey,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w400),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also",
                style: TextStyle(
                    fontSize: 13.0,
                    color: Colors.black,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w300),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Icon(Icons.location_on),
                    ),
                    Container(
                      child: Text("Surabaya"),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Icon(Icons.phone),
                    ),
                    Container(
                      child: Text("+6287992783672"),
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
