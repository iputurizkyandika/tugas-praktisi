import 'setting.dart';
import 'package:flutter/material.dart';
import 'tokosaya.dart';

void main() {
  runApp(new MaterialApp(home: new Beranda(), routes: <String, WidgetBuilder>{
    '/tokosaya': (BuildContext context) => new TokoSaya(),
    '/beranda': (BuildContext context) => new Beranda(),
    '/setting': (BuildContext context) => new Setting()
  }));
}

class Beranda extends StatelessWidget {
  const Beranda({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        backgroundColor: Colors.deepPurple[300],
      ),
      body: Center(
        child: Text("Home"),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.deepPurple[300],
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.home_rounded,
                color: Colors.white,
              ),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.add_shopping_cart_rounded, color: Colors.white),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.shop_2, color: Colors.white),
              onPressed: () {
                Navigator.pushNamed(context, '/tokosaya');
              },
            ),
            IconButton(
              icon: Icon(Icons.person_outlined, color: Colors.white),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
